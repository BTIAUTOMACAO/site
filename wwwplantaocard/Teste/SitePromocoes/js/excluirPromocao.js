 $(function(){

	var query = location.search.slice(1);
	var partes = query.split('&');
	var data = {};
	partes.forEach(function (parte) {
	    var chaveValor = parte.split('=');
	    var chave = chaveValor[0];
	    var valor = chaveValor[1];
	    data[chave] = valor;
	    sessionStorage.setItem('id', valor);
	});

	BuscarDados();
})

 function  BuscarDados(){

 	$.ajax({
 		type: "GET",
 		url: sessionStorage.getItem('endereco') + "Promocao/BuscaPromocaoPorIdeCredId",
 		data:{	
 			id: sessionStorage.getItem('id'),
 			credID:  sessionStorage.getItem('credID')
 		},
 		success: function (data) {
 			if(data[0].codErro == "00"){
 				$.each(data, function (i) {
 					$("#titulo").val(data[i].Titulo);
 					$("#categoria").val(data[i].Categoria);
 					$("#nome").val(data[i].Solicitante);
 					$("#rg").val(data[i].RG);
 					$("#dataCadastro").val(data[i].DataCadastro);
 					if(data[i].Liberado == 'S')
 					{
 						$("#liberado").prop('checked', true);
 					}
 					else
 						{	$("#liberado").prop('checked', false);
 				}
 			})
 			}else{
 				alert(data[0].mensagem,"Atenção");
 			}			
 		},
 		error: function(){
 			alert("Erro ao tentar acessar o servidor","Atenção");
 		}

 	})

 }

 function ExcluirPromocao()
{
 	$.ajax({
 		type: "GET",
 		url: sessionStorage.getItem('endereco') + "Promocao/ExcluirPrmocao",
 		data:{	
 			credID:sessionStorage.getItem('credID'),
 			tipo: sessionStorage.getItem('tipo'),
 			id: sessionStorage.getItem('id')
 		},
 	    success: function (data) {
	   		alert("Promoção Excluída com Sucesso!","Atenção");	
	   		Limpar();
	   	},
 		error: function(){
 			alert("Erro ao tentar acessar o servidor","Atenção");
 		}

 	})
};

function Limpar()
{
    $("input[type=text]").val("");
    $("input[type=date]").val("");
    $('input[type=checkbox]').prop('checked', false);
}