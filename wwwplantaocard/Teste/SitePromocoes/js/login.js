 $(function(){
 	$("#codAcesso").val("");
 	$("#senha").val("");
 })


$("#btnEntrar").click(function() {

	$("#mensagem").hide();
	if($("#codAcesso").val() == ""){
			alert("Cód. de Acesso não pode estar em branco","Atenção");
	}else if($("#senha").val() == ""){
			alert("Senha não pode estar em branco","Atenção");
	}else{
		$('#load-screen').show();

		sessionStorage.setItem('endereco', "http://ts1.drogabella.com/apiTesteEstoqueFilial/api/");

		var codAcesso = $("#codAcesso").val();
		var senhaUsuario = $("#senha").val();
		Login(codAcesso, senhaUsuario);
	}
});


function Login(codAcesso,senhaUsuario){
	$('#load-screen').show();
	$.ajax({
		type: "POST",
		    url: sessionStorage.getItem('endereco') + "Login/Logar",
		    data:
		    {
		    	CodAcesso: codAcesso,
		    	Senha: senhaUsuario,
		    	Tipo: 'P'
	        },
		    success: function (data) {
		    	$('#load-screen').hide();
		    	if (data[0].codErro == "00"){
		    		sessionStorage.setItem('codAcesso', codAcesso);
                    sessionStorage.setItem('senha', senhaUsuario);
                    sessionStorage.setItem('tipo', 'P');
                    sessionStorage.setItem('nome', data[0].nome);
                    sessionStorage.setItem('fantasia', data[0].fantasia);
                    sessionStorage.setItem('credID', data[0].credId);
		    		window.location.assign("principal.html");
		    	}
		    	else{
		    		$("#codAcesso").val("");
 					$("#senha").val("");
		    		$("#mensagem").show();
		    		$("#mensagem").html(data[0].mensagem);
		    	}
		    },
		    error: function(data){
		    	alert(data,'Erro');
		    	$('#load-screen').hide();
		    }
		})

}

