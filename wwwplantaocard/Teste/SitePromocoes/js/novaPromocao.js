var marcado = false;
var promocaoFixa = "N";


$(function(){
	Limpar();
})

$('#periodoFixo').change(function() {
    if(this.checked != true){
        $('#dataLabel').attr("hidden", true);
        $("#dataInicio").attr("hidden", true);
        $('#dataLabelFim').attr("hidden", true);
        $("#dataFim").attr("hidden", true);
        marcado = false;
     }else{
        $('#dataLabel').attr("hidden", false);
        $("#dataInicio").attr("hidden", false);
        $('#dataLabelFim').attr("hidden", false);
        $("#dataFim").attr("hidden", false);
        marcado = true;
    }
});

function ValidaDadosPromocao()
{
 	if($("#titulo").val() == ""){
 		alert("Titulo não pode ser em Branco!","Atenção");
    }else if($("#categoria").val() == ""){
        alert("Categoria não pode ser em Branco!","Atenção");
    }else if($("#nome").val() == ""){
        alert("Solicitante não pode ser em Branco!","Atenção");
    }else if($("#rg").val() == ""){
        alert("RG não pode ser em Branco!","Atenção");
    }else if($("#descricao").val() == ""){
        alert("Descricao não pode ser em Branco!","Atenção");
 	}else if($('#periodoFixo:checked').val() != undefined)
 	{
 		promocaoFixa = "S";
 		if($("#dataInicio").val() == "" || $("#dataFim").val() == "")
 		{
 			alert("Data não pode ser em Branco!","Atenção");
 		}
 		else
 		{
        	var dataComp = new Date($("#dataInicio").val());
            var dataFimPromo = new Date($("#dataFim").val());
        	if(dataFimPromo < dataComp)
            {
                alert("Data Final não pode ser menor que a Inicial!","Atenção"); 
            }
	        else if($("#arquivo").val() == "")
			{
				 alert("Selecione o arquivo da Promoção!","Atenção"); 
			}
			else
			{
				var file = document.getElementById('arquivo').files[0];
				SalvarPromocao($("#titulo").val(), $("#descricao").val(), promocaoFixa,$("#dataInicio").val(),$("#dataFim").val(),
                    file.name, $("#nome").val(), $("#rg").val(), $("#categoria").val());
			}
 		}
 	}else if($("#arquivo").val() == "")
	{
		alert("Selecione o arquivo da Promoção!","Atenção"); 
	}
 	else{
 		var file = document.getElementById('arquivo').files[0];
 		SalvarPromocao($("#titulo").val(), $("#descricao").val(), promocaoFixa,$("#dataInicio").val(),$("#dataFim").val(),
                    file.name, $("#nome").val(), $("#rg").val(), $("#categoria").val());
 	}
}

function SalvarPromocao(promoTitulo, promoDescricao, promoTipo, promoDataInicio, promoDataFim, promoArquivo, solicitante, rg, categoria)
{
 	$.ajax({
 		type: "POST",
 		url: "http://ts1.drogabella.com/testeapi/api/Promocao/CadastrarPromocao",
 		data:{	
 			Titulo: promoTitulo,
 			Descricao: promoDescricao,
 			PeriodoFixo: promoTipo,
 			DataInicio: promoDataInicio,
            DataFim: promoDataFim,
 			Arquivo: promoArquivo,
 			Tipo: sessionStorage.getItem('tipo'),
 			CredID: sessionStorage.getItem('credID'),
 			Liberado: 'S',
            Solicitante: solicitante,
            RG: rg,
            Categoria: categoria
 		},
 		success: function (data) {
 			if(data[0].codErro == "00"){
 				uploadFile();
                Limpar();
                alert("VOUCHER DA PROMOÇÃO: " + data[0].Voucher,"Atenção");
 			}else{
 				alert(data[0].mensagem,"Atenção");
                Limpar();
 			}			
 		},
 		error: function(){
 			alert("Erro ao tentar acessar o servidor","Atenção");
 		}

 	})
}

 function uploadFile() {        
        var xhr = new XMLHttpRequest();                 
        var file = document.getElementById('arquivo').files[0];
        xhr.open("POST", "http://ts1.drogabella.com/testeapi/api/Promocao/UploadPromocao");
        xhr.setRequestHeader("filename", file.name);
        xhr.send(file);


    }

function Limpar()
{
    $('#dataLabel').attr("hidden", true);
    $("#dataInicio").attr("hidden", true);
    $('#dataLabelFim').attr("hidden", true);
    $("#dataFim").attr("hidden", true);
    $("input[type=text]").val("");
    $("input[type=date]").val("");
    document.getElementById("descricao").value = "";
    $("input[type=file]").val("");
    $('input[type=checkbox]').prop('checked', false);
}