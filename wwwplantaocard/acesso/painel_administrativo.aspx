﻿<%@ Page Language="C#" EnableEventValidation = "false" AutoEventWireup="true" CodeBehind="painel_administrativo.aspx.cs" Inherits="PlantaoCard.painel_administrativo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <title>Cadastro de Promoções</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- CSS -->
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=PT+Sans:400,700'>
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Oleo+Script:400,700'>
        <link rel="stylesheet" href="../css/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/assets/css/style.css">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- LIMPAR HISTÓRICO PARA NÃO DEIXAR VOLTAR PARA A PÁGINA DE CONTROLE -->
        <script language="javascript" type="text/javascript">
            history.forward();
        </script>
        <!-- FORMATAR O CAMPO DATA -->
        <script type="text/javascript">
            function mascara_data(campo) {
                if (campo.value.length == 2) {
                    campo.value += '/';
                } if (campo.value.length == 5) {
                    campo.value += '/';
                    }
                }
        </script>
        <!-- LIMITAR CARACTERES DO CAMPO DESCRIÇÃO -->
        <script type="text/javascript">
            function ismaxlength(obj) {
                var mlength = obj.getAttribute ? parseInt(obj.getAttribute("maxlength")) : ""
                if (obj.getAttribute && obj.value.length > mlength) {
                    obj.value = obj.value.substring(0, mlength)
                }
            }
        </script>
        <!-- LIMITAR CAMPO PARA RECEBER APENAS NÚMEROS -->
        <script type="text/javascript"> 
            function PermiteSomenteNumeros(event) 
            {
	            var charCode = (event.which) ? event.which : event.keyCode 
	            if (charCode > 31 && (charCode < 48 || charCode > 57)) 
		            return false; 
	            return true; 
            }
        </script>
        </head>
    <body>
        <form id="form1" runat="server">
        <!-- MENU -->
        <div style="margin: 15px; background: #DBDBDB; padding:6px; border-radius: 5px;">
            <asp:Button ID="btnInicio" runat="server" Text="Promoções" CssClass="botao" 
                onclick="btnInicio_Click"/>
            <asp:Button ID="btnSair" runat="server" Text="Sair" CssClass="botao" 
                onclick="btnSair_Click"/>
            <asp:Button ID="btnCadastroUsuario" runat="server" Text="Cadastro Usuário" 
                CssClass="botao" onclick="btnCadastroUsuario_Click" Visible="False" 
                />
            <div style="float:right; margin: 10px">
                <strong>Usuário:</strong> <asp:Label ID="lblNome" runat="server"></asp:Label>
            </div>
        </div>
        <!-- /MENU -->
        
        <!-- CADASTRO DA PROMOÇÃO -->
        <div class="register-container container">
            <div class="row">
                <div class="iphone span5">
                    <img src="../css/assets/img/iphone.png" alt="">
                </div>
                <div class="register span6">
                        <h2>Cadastre á sua<span class="red"><strong> Promoção<asp:TextBox ID="txtIdProm" 
                                runat="server" Visible="False"></asp:TextBox>
                            </strong></span></h2>
                        <label for="titulo">Título</label>
                        <asp:TextBox ID="txtTitulo" runat="server" Width="323px"></asp:TextBox>

                        <label for="titulo">Descrição</label>
                        <asp:TextBox ID="txtDescricao" runat="server" TextMode="MultiLine" 
                            Width="323px" MaxLength="140" Height="84px" Rows="3"></asp:TextBox>

                        <label for="data">Data final</label>
                        <asp:TextBox ID="txtData" runat="server" Width="323px" 
                            onKeyUp="mascara_data(this)" MaxLength="10" onkeypress="return PermiteSomenteNumeros(event);"></asp:TextBox>

                        <label for="estado">Estado</label>
                        <asp:DropDownList ID="dlEstado" runat="server" Width="336px">
                            <asp:ListItem>AC</asp:ListItem>
                            <asp:ListItem>AL</asp:ListItem>
                            <asp:ListItem>AP</asp:ListItem>
                            <asp:ListItem>AM</asp:ListItem>
                            <asp:ListItem>BA</asp:ListItem>
                            <asp:ListItem>CE</asp:ListItem>
                            <asp:ListItem>DF</asp:ListItem>
                            <asp:ListItem>ES</asp:ListItem>
                            <asp:ListItem>GO</asp:ListItem>
                            <asp:ListItem>MA</asp:ListItem>
                            <asp:ListItem>MT</asp:ListItem>
                            <asp:ListItem>MS</asp:ListItem>
                            <asp:ListItem Text="MG" Value="MG"/>
                            <asp:ListItem>PA</asp:ListItem>
                            <asp:ListItem>PB</asp:ListItem>
                            <asp:ListItem Text="PR" Value="PR"/>
                            <asp:ListItem>PE</asp:ListItem>
                            <asp:ListItem>PI</asp:ListItem>
                            <asp:ListItem>RJ</asp:ListItem>
                            <asp:ListItem>RN</asp:ListItem>
                            <asp:ListItem>RS</asp:ListItem>
                            <asp:ListItem>RO</asp:ListItem>
                            <asp:ListItem>RR</asp:ListItem>
                            <asp:ListItem>SC</asp:ListItem>
                            <asp:ListItem>SP</asp:ListItem>
                            <asp:ListItem>SE</asp:ListItem>
                            <asp:ListItem>TO</asp:ListItem>
                        </asp:DropDownList>
                        
                        <label for="cidade">Cidade</label>
                        <asp:TextBox ID="txtCidade" runat="server" Width="323px"></asp:TextBox>
                        
                        <label for="cidade">Imagem <strong>(.jpg)</strong></label>
                        <asp:FileUpload ID="fileImagem" runat="server" class="multi" 
                            AllowMultiple="True" />
                        
                        <hr />
                        <asp:Button ID="btnCadastrar" runat="server" Text="Cadastrar" CssClass="botao" 
                            onclick="btnCadastrar_Click" />
                        <asp:Button ID="btnApagar" runat="server" Text="Apagar" CssClass="botao" 
                            Visible="False" onclick="btnApagar_Click"/>
                        <asp:Button ID="btnAlterar" runat="server" Text="Alterar" CssClass="botao" 
                            Visible="False" onclick="btnAlterar_Click"/>
                        <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" 
                            CssClass="botao" Visible="False" onclick="btnCancelar_Click"/>
                        
                        <asp:Label ID="lblAviso" runat="server" ForeColor="#CC3300" Visible="False"></asp:Label>
                </div>
            </div>
        </div>
        <!-- /CADASTRO DA PROMOÇÃO -->

        <!-- PAINEL COM PROMOÇÕES CADASTRADAS -->
        <center>
            <h2>Promoções Cadastradas</h2>
            <!-- TABELA -->
            <asp:GridView ID="gvPromocoes" runat="server" AutoGenerateColumns="False" 
                BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" 
                CellPadding="3" EnableModelValidation="True" GridLines="Vertical" 
                onselectedindexchanged="gvPromocoes_SelectedIndexChanged" 
                onrowdatabound="gvPromocoes_RowDataBound" >
                <AlternatingRowStyle BackColor="Gainsboro"/>
                <Columns>
                    <asp:BoundField DataField="idpromocoes" HeaderText="ID"/>
                    <asp:BoundField DataField="titulo" HeaderText="Título" />
                    <asp:BoundField DataField="descricao" HeaderText="Descrição" />
                    <asp:BoundField DataField="data" HeaderText="Data Final" />
                    <asp:BoundField DataField="estado" HeaderText="Estado" />
                    <asp:BoundField DataField="cidade" HeaderText="Cidade" />
                    <asp:BoundField DataField="nomeusuario" HeaderText="Usuário" />
                    <asp:ButtonField ButtonType="Button" Text="Selecionar" CommandName="Select" 
                        HeaderText="Alterar / Apagar" />
                </Columns>
                <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                <HeaderStyle BackColor="#3C6BC3" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            </asp:GridView>
            <asp:HiddenField ID="hdId" runat="server" />
        </center>
        <!-- /PAINEL COM PROMOÇÕES CADASTRADAS -->

        <!-- JAVASCRIPT -->
        <script src="../css/assets/js/jquery-1.8.2.min.js"></script>
        <script src="../css/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="../css/assets/js/jquery.backstretch.min.js"></script>
        <script src="../css/assets/js/scripts.js"></script>
        <!-- /JAVASCRIPT -->
        </form>
    </body>
</html>