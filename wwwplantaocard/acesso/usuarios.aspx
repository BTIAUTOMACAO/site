﻿<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="usuarios.aspx.cs"
    Inherits="PlantaoCard.acesso.usuarios" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <title>Cadastro de Usuários</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- CSS -->
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=PT+Sans:400,700'>
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Oleo+Script:400,700'>
    <link rel="stylesheet" href="../css/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/assets/css/style.css">
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    <!-- LIMPAR HISTÓRICO PARA NÃO DEIXAR VOLTAR PARA A PÁGINA DE CONTROLE -->
    <script language="javascript" type="text/javascript">
        history.forward();
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <!-- MENU -->
    <div style="margin: 15px; background: #DBDBDB; padding: 6px; border-radius: 5px;">
        <asp:Button ID="btnInicio" runat="server" Text="Promoções" CssClass="botao" OnClick="btnInicio_Click" />
        <asp:Button ID="btnSair" runat="server" Text="Sair" CssClass="botao" OnClick="btnSair_Click" />
        <asp:Button ID="btnVoltar" runat="server" Text="Voltar" CssClass="botao" OnClick="btnVoltar_Click" />
        <div style="float: right; margin: 10px">
            <strong>Usuário:</strong>
            <asp:Label ID="lblNome" runat="server"></asp:Label>
        </div>
    </div>
    <!-- FORMULARIO DE CADASTRO DE USUARIO -->
    <div class="register-container container">
        <div class="row">
            <div class="iphone span5">
                <img src="../css/assets/img/usuario.png" alt="">
            </div>
            <div class="register span6">
                <h2>
                    Cadastre um<span class="red"><strong> Usuário</strong></span></h2>
                
                <asp:TextBox ID="txtID" runat="server" Visible="false"></asp:TextBox>
                <label for="Nome">Nome</label>
                <asp:TextBox ID="txtNome" runat="server"  Width="323px"></asp:TextBox>

                <label for="Nome">Senha</label>
                <asp:TextBox ID="txtSenha" runat="server"  Width="323px" TextMode="Password"></asp:TextBox>

                <label for="Nome">Empresa</label>
                <asp:TextBox ID="txtEmpresa" runat="server"  Width="323px"></asp:TextBox>
                
                <hr />
                <asp:Button ID="btnCadUsuario" runat="server" Text="Cadastrar" CssClass="botao" 
                    onclick="btnCadUsuario_Click"/>

                <asp:Button ID="btnApagar" runat="server" Text="Apagar" CssClass="botao" 
                    Visible="False" onclick="btnApagar_Click" />
                <asp:Button ID="btnAlterar" runat="server" Text="Alterar" CssClass="botao" 
                    Visible="False" onclick="btnAlterar_Click" />
                <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CssClass="botao" 
                    Visible="False" onclick="btnCancelar_Click" />

                <asp:Label ID="lblAvisoUsuario" runat="server" ForeColor="#CC3300" Visible="False"></asp:Label>
                <br /><br />
                <asp:Label ID="lblAviso" runat="server" ForeColor="#CC3300" Visible="False"></asp:Label>

            </div>
        </div>
    </div>
    <!-- /FORMULARIO DE CADASTRO DE USUARIO -->
    
    <!-- GRID COM USUARIOS CADASTRADOS -->
    <center>
        <h2>Usuários Cadastrados</h2>
        <asp:GridView ID="gvUsuarios" runat="server" AutoGenerateColumns="False" 
            BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" 
            CellPadding="3" EnableModelValidation="True" GridLines="Vertical" 
            onrowdatabound="gvUsuarios_RowDataBound" 
            onselectedindexchanged="gvUsuarios_SelectedIndexChanged" >
            <AlternatingRowStyle BackColor="#DCDCDC" />
            <Columns>
                <asp:BoundField DataField="idusuarios" HeaderText="ID" />
                <asp:BoundField DataField="nome" HeaderText="Usuários" />
                <asp:BoundField DataField="empresa" HeaderText="Empresas" />
                <asp:ButtonField ButtonType="Button" Text="   Selecionar    " CommandName="Select" 
                    HeaderText="Alterar / Apagar" />
            </Columns>
            <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
            <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
        </asp:GridView>
        <asp:HiddenField ID="hdId" runat="server" />
    </center>
    <!-- GRID COM USUARIOS CADASTRADOS -->

    <!-- JAVASCRIPT -->
    <script src="../css/assets/js/jquery-1.8.2.min.js"></script>
    <script src="../css/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="../css/assets/js/jquery.backstretch.min.js"></script>
    <script src="../css/assets/js/scripts.js"></script>
    <!-- /JAVASCRIPT -->
    </form>
</body>
</html>
