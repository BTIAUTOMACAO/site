 $(function(){

	var query = location.search.slice(1);
	var partes = query.split('&');
	var data = {};
	partes.forEach(function (parte) {
	    var chaveValor = parte.split('=');
	    var chave = chaveValor[0];
	    var valor = chaveValor[1];
	    data[chave] = valor;
	    sessionStorage.setItem('credID', valor);
	});

	sessionStorage.setItem('tipo', 'P');
    sessionStorage.setItem('endereco', "http://ts1.drogabella.com/testeapi/api/");

    if(sessionStorage.getItem('codAcesso') == "")
    {
    	Login();
	}
})


function Login(){
	$('#load-screen').show();
	$.ajax({
		type: "POST",
		    url: sessionStorage.getItem('endereco') + "Login/Logar",
		    data:
		    {
		    	CredID: sessionStorage.getItem('credID'),
		    	Tipo: 'P'
	        },
		    success: function (data) {
		    	$('#load-screen').hide();
		    	if (data[0].codErro == "00"){
                    sessionStorage.setItem('tipo', 'P');
                    sessionStorage.setItem('credID', data[0].credId);
                    sessionStorage.setItem('nome', data[0].nome);
                    sessionStorage.setItem('fantasia', data[0].fantasia);
                    sessionStorage.setItem('codAcesso', data[0].codAcesso);
		    	}
		    },
		    error: function(data){
		    	alert(data,'Erro');
		    }
		})

}