﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="GestaoEscolar.geEscola.index" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Gestão Escolar - BTI</title>
    <link rel="stylesheet" type="text/css" href="../css/960.css" />
    <link rel="stylesheet" type="text/css" href="../css/reset.css" />
    <link rel="stylesheet" type="text/css" href="../css/text.css" />
    <link rel="stylesheet" type="text/css" href="../css/blue.css" />
    <script type="text/javascript" src="../../ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
    <script type="text/javascript" src="../js/ui.core.js"></script>
    <script type="text/javascript" src="../js/ui.sortable.js"></script>
    <script type="text/javascript" src="../js/ui.dialog.js"></script>
    <script type="text/javascript" src="../js/ui.datepicker.js"></script>
    <script type="text/javascript" src="../js/effects.js"></script>
    <script id="source" language="javascript" type="text/javascript" src="/js/graphs.js"></script>
</head>
<body>
    <form id="Form1" runat="server">
    <div class="container_16" id="wrapper">
        <div class="grid_8" id="logo">
            Painel da Escola</div>
        <div class="grid_8">
            <!-- TOPO LADO DIREITO -->
            <div style="font-size: large; color: White; float: right; padding-top: 20px">
                Bem vindo 
                <asp:Label ID="lblUsuario" runat="server" Text=""></asp:Label>
                | 
                <asp:Button ID="btnSair" runat="server" Text="Sair" onclick="btnSair_Click" /></div>
            <div id="user_tools">
                <span><a href="#"></a></span>
            </div>
        </div>
        <div class="grid_16" id="header">
            <!-- MENU -->
            <div id="menu">
                <ul class="group" id="menu_group_main">
                    <li class="item first" id="one"><a href="#" class="main"><span class="outer"><img src="../images/icon_dashboard.png" onMouseOver="this.src='../images/icon_dashboard_mouse.png'" onmouseout="this.src='../images/icon_dashboard.png'"/><br />Frequência<br /> e Notas</span></span></a></li>
                    <li class="item middle" id="two"><a href="#" class="main"><span class="outer"><img src="../images/icon_media.png" onMouseOver="this.src='../images/icon_media_mouse.png'" onmouseout="this.src='../images/icon_media.png'"/><br />Extrato do<br />Cartão Cantina</span></a></li>
                    <li class="item middle" id="three"><a href="#" class="main"><span class="outer"><img src="../images/icon_reports.png" onMouseOver="this.src='../images/icon_reports_mouse.png'" onmouseout="this.src='../images/icon_reports.png'"/><br />Boletos</span></a></li>
                    <li class="item middle" id="four"><a href="biblioteca.aspx" class="main"><span class="outer"><img src="../images/icon_users.png" /><br />Biblioteca</span></a></li>
                    <li class="item middle" id="five"><a href="noticia.aspx" class="main"><span class="outer"><img src="../images/icon_news.png" /><br />Notícias</span></a></li>
                    <li class="item middle" id="six"><a href="psicoPedagogico.aspx" class="main"><span class="outer"><img src="../images/icon_clock.png" /><br />Apoio<br />PsicoPedagógico</span></a></li>
                    <li class="item middle" id="seven"><a href="formulario.aspx" class="main"><span class="outer"><img src="../images/icon_edit.png" /><br />Formulário de<br />Solicitações</span></a></li>
                    <li class="item last" id="eight"><a href="http://btiautomacao.com.br/moodle/" target="_blank" class="main"><span class="outer"><img src="../images/icon_settings.png" /><br />Ambiente<br />Virtual</span></a></li>
                </ul>
            </div>
            <!-- /MENU -->
        </div>
        <div class="grid_16">
            <!-- BOTÃO INICIAL -->
            <div id="tabs">
                <div class="container">
                    <ul>
                        <li><a href="index.aspx" class="current"><span>Início</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="grid_16" id="content">
            <!--  MENU INICIO  -->
            <div class="grid_9">
                <h1 class="dashboard">
                    Início</h1>
            </div>
            <div class="grid_6" id="eventbox">
                <a href="index.aspx" class="inline_calendar" style="padding-bottom: 5px;">
                    <asp:Label ID="lblData" runat="server" Text=""></asp:Label></a>
                <div class="hidden_calendar">
                </div>
            </div>
            <div class="clear">
            </div>
            <!--  IMAGEM DA HOME -->
            <img src="../images/colegio.jpg"
            <div id="portlets">
                <div class="column" id="left">
                    <!--NOTICIA-->
                    <div class="portlet">
                        <div class="portlet-header">
                            <img src="../images/icons/feed.gif" width="16" height="16" alt="Feeds" />Notícias
                        </div>
                        <div class="portlet-content">
                            <ul class="news_items">
                                <li>
                                    O Portal de Gestão Escolar é uma ferramenta inovadora e bastante eficaz que permitirá 
                                    aos alunos da rede estadual e os seus responsáveis acessarem informações como histórico 
                                    escolar, boletins e horário de aulas. Tudo sem sair de casa.
                                </li>
                                <li>
                                    Acessado por meio de senha, o Portal Educacional é a mais nova ferramenta que a Seduc 
                                    cria como canal entre a sociedade e os serviços da secretaria. As informações cadastrais 
                                    dos alunos também poderão ser atualizadas diretamente no site.
                                </li>
                                <li>
                                    Com o Portal Educacional, os pais poderão, sempre que desejarem, visualizar as notas dos 
                                    filhos pela Internet, sem ter que ir até as escolas. Futuramente, mais serviços serão 
                                    disponibilizados, como a solicitação de declarações, transferência e histórico escolar.
                                </li>
                                <li>
                                    Isso significa que o aluno, mãe ou pai que precisar de uma declaração de que o estudante 
                                    está regularmente matriculado, por exemplo, poderá fazer a solicitação do documento via 
                                    Internet. O pedido será recebido diretamente pela secretaria da escola, e o responsável 
                                    se dirigirá à unidade de ensino somente para receber a documentação.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <br />
                <!-- AVISOS -->
                <div class="column">
                    <div class="portlet">
                        <div class="portlet-header">
                            <img src="../images/icons/comments.gif" width="16" height="16" alt="Comments" />Avisos</div>
                        <div class="portlet-content">
                            <p class="info" id="success">
                                <span class="info_inner">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</span></p>
                            <p class="info" id="error">
                                <span class="info_inner">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</span></p>
                            <p class="info" id="warning">
                                <span class="info_inner">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</span></p>
                            <p class="info" id="info">
                                <span class="info_inner">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</span></p>
                            </div>
                    </div>
                </div>
                <div class="clear">
                </div>
            </div>
            <div class="clear">
            </div>
            <!-- FIM DOS AVISOS -->
        </div>
        <div class="clear">
        </div>
    </div>
    <!-- FOOTER -->
    <div class="container_16" id="footer">
        Gestão Escolar <a href="index.aspx">BTI</a></div>
    <!-- FIM FOOTER -->
    </form>
</body>
</html>