<?php



?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="INDEX, FOLLOW" / >
    <meta name="language" content="portuguese" />
    <meta name="document-rights" content="Public" / >
    <meta name="document-rating" content="General" / >
    <meta name="document-state" content="Dynamic" / >
    <meta http-equiv="Content-Language" content="PT-BR" / >
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href='http://fonts.googleapis.com/css?family=Pontano+Sans' rel='stylesheet' type='text/css'>
    <meta name="description" content="<?=$var_description;?>">
    <meta name="keywords" content="<?=$palavra_chave;?>">
    <script type="text/javascript" src="assets/js/jquery-1.7.1.min.js"></script>
    <link rel="shortcut icon" type="image/ico" href="favicon.ico" />
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="assets/css/banner.css">
    <link rel="stylesheet" type="text/css" href="assets/css/topo_1.css">
    <link rel="stylesheet" type="text/css" href="assets/css/geral.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400" rel="stylesheet">
    <!-- <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet"> -->
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <script src="assets/js/jssor.slider-23.0.0.min.js" type="text/javascript"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDmsCjVC09sZS4qG87O1jE1rCCz42xgNsA" type="text/javascript"></script>
    <script src="assets/js/banner.js" type="text/javascript"></script>

    <!-- <link rel="stylesheet" type="text/css" href="assets/css/sreset.css"> -->
    <script src="assets/js/jquery-latest.js"></script>
    <script src="assets/js/js.js"></script>

        <title>Plantão Card</title>


    <style type="text/css">

    .ttle_default{
      /*font-size: 38px;*/
      /*text-align: center;*/
      font-family: 'Raleway', sans-serif;
    }

    .ttle_default_sub{
      font-size: 20px;
      text-align: center;
      font-family: 'Raleway', sans-serif;
    }


    .int_map{
      position: absolute;
      z-index: 1000;
      background-color: #fff;
      padding:10px;
      margin-top: 50px;
      margin-left: 10px;
      border-radius:2px;
      border-color: #080808;
      border:none;
      -webkit-box-shadow: 0px 0px 20px rgba(0, 0, 0, 0.3);
      -moz-box-shadow:    0px 0px 20px rgba(0, 0, 0, 0.3);
      box-shadow:         0px 0px 20px rgba(0, 0, 0, 0.3);

    }

    .ttle_Card{
      font-family: 'Raleway', sans-serif;
      font-weight: bold;
      font-size: 30px;
      color: #999;
    }

    .p_padrao{
      padding-bottom: 40px;
      padding-top: 40px;
    }

    .p_0{
      padding: 0;
    }

    .home_res{
      max-width: 1364px;
    }

    .img_responsa{
      width: 100%;
    }



    .bg_fff{
      background-color: #fff;
    }

    .font_25{
      font-size: 25px;
    }

    .font_15{
      color: #787878;
      font-size: 15px;
    }

    .span_1{
      color: #483c8c;
      font-weight: bold;
    }

    .span_2{
      color: #787878;
      font-weight: ;
    }

    .home_text{
      height: 500px;
    }

    .campo{
      height: 45px;
      text-indent: 0.8em;
      width: 100%;
      border-radius: 3px;
      background-color: #fff;
      border:#e5e5e5 solid 1px;
      font-size: 14px;
      margin-bottom: 10px;
    }.campo:focus{
      outline: none;
      border:#e0e0e0 solid 1px;
      background-color: #efefef;
    }
        .btn_contact{

        width: 100%;

        color: #707070;

        padding: 10px;
        border-radius: 2px;
        cursor: pointer;

    }.btn_contact:hover{
        opacity: 0.8;
    }

    .ttl_form{
      font-size: 18px;
      color: #666;
    }

    .campo_text{
      padding-top: 8px;
      padding-bottom: 8px;
      text-indent: 0.8em;
      width: 100%;
      height: 100px;
      border-radius: 3px;
      background-color: #fff;
      border:#e5e5e5 solid 1px;
    }.campo_text:focus{
      outline: none;
      background-color: #efefef;
      border:#e0e0e0 solid 1px;
    }

    .ttle_form{
      font-size: 25px;
      font-weight: bold;
      color: #1d2886;
    }


    </style>



    <script>
    

    </script>


    </head>





<body>



    <?php include("_topo.php");?>


  <div class='container-fluid text-center bg_fff p_padrao'>
        <div class='container'>
          <div class='row text-center'>
            <div class='col-md-12'>
              <table>
                  <td style="padding:0 15px 0 15px;"><p>Segmento:  </p></td><td><p><select id="segmento">
                  <option value="0">Todos</option></select></p></td>
                  <td style="padding:0 15px 0 15px;"><p>Estabelecimento:  </p></td><td colspan="6"><p><input type="text" class="form-control" id="estabelecimento">
                  </p></td>
                  <td style="padding:0 15px 0 15px;"><p>Categoria:  </p></td><td><p><select id="categoria">
                  <option value="0">Todas</option></select></p></td>
                  <td style="padding:0 15px 0 15px;"><p>Cidade:  </p></td><td><p><select id="cidade">
                  <option value="0">Todas</option></select></p></td>
                  <td style="padding:0 15px 0 15px;"><p></p></td>
                  <td colspan="4"><button type="button" class="btn btn-primary btn-lg" onclick="Buscar()">BUSCAR</button></td>
                  </tr>
                  </table>
                 
            </div>
          </div>
        </DIV>

      </div>


<div class="container" id="listaPromocoes">
   
</div>

  <div class='container-fluid p_padrao' style='background-color:#e5e5e5;'>
      <div class='container'>
        <div class='col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center font_25'><span class='span_1'>Cadastre sua Promoção para ser Visualizada em Nossas Midías Digitais.<br>Entre em contato com a Administradora para maiores informações.<bR>Contato: (12)3938-3633.</span></div>
      </div>
  </div>


    <?php include("footer.php");?>



</body>
</html>

<script src="assets/js/promocao.js"></script>
<script src="assets/js/bootstrap_2.min.js"></script>
<script type="text/javascript">jssor_1_slider_init();</script>


