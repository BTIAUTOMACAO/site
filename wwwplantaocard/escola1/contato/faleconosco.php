<!DOCTYPE html>
<html>
<head>
 	<meta charset="utf8">
	<title>
		Contato - Plantão Card Alimentação
	</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<script type="text/javascript" src="jquery-3.2.0.min.js"/></script>
	<script type="text/javascript" src="jquery.mask.min.js"/></script>
	<script type="text/javascript" src="contato.js"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>
	<div class="container-fluid">
		<nav class="navbar navbar-default">
			<a href="">
				<img src="logo.png" alt="">

			</a>
		</nav>

		<div class="flex-row">
		  <div class="col-xs-6" style="">
		  	<form class="form-horizontal" id="formContato">
					  <div class="form-group">
		  			  	<h2>Informe os Dados Abaixo</h2>	
		  			  	<h4>Em breve um representante entrará em contato com você!</h4>
		  			  </div>
                      <div class="form-group">
                        <label for="razaoSocial">Razão Social/Nome Fantasia</label>
                        <input type="text" required="true" name="razaoSocial" class="form-control" id="razaoSocial" placeholder="Razão social ou nome fantasia da empresa">
                      </div>
                      <div class="form-group">
                        <label for="Contato">Contato</label>
                        <input type="text" required="true" name="contato" class="form-control" id="contato" placeholder="Nome para contato">
                      </div>
                      <div class="form-group">
                        <label for="email">E-mail</label>
                        <input type="text" required="true" name="email" class="form-control" id="email" placeholder="empresa@empresa.com.br">
                      </div>
                      <div class="form-group">
                        <label for="telefone">Telefone</label>
                        <input type="text" required="true" name="telefone" class="form-control" id="txttelefone" pattern="\([0-9]{2}\)[\s][0-9]{4}-[0-9]{4,5}" placeholder="(XX) XXXX-XXXXX">
                      </div>
                      <!-- <div class="form-group">
                        <label for="cnpj">CNPJ</label>
                        <input type="text" class="form-control" id="cnpj" placeholder="">
                      </div>
                      <div class="form-group">
                        <label for="ie">IE</label>
                        <input type="text" class="form-control" id="ie" placeholder="">
                      </div>
                      <div class="form-group">
                        <label for="endereco">Endereço</label>
                        <input type="text" class="form-control" id="endereco" placeholder="">
                      </div>
                      <div class="form-group">
                        <label for="Nome">Nome</label>
                        <input type="text" class="form-control" id="nome" placeholder="">
                      </div>
                      <div class="form-group">
                        <label for="email">E-mail</label>
                        <input type="text" class="form-control" id="email" placeholder="empresa@examplo.com">
                      </div>
                      <div class="form-group">
                        <label for="tel">Telefone</label>
                        <input type="text" class="form-control" id="telefone" placeholder="">
                      </div>
                      <div class="form-group ">
                        <label for="dataDoCredito">Data do Crédiito</label>
                       <input type="date"  class="form-control" placeholder="Description"></textarea> 
                      </div>
                      <div class="form-group">
                        <label for="dataPagamento">Data de Pagamento</label>
                        <input type="date" class="form-control" id="exampleInputEmail2" placeholder="">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail2">CNPJ</label>
                        <input type="email" class="form-control" id="exampleInputEmail2" placeholder="jane.doe@example.com">
                      </div> -->
                      <div class="form-group ">
                        <!-- <input type="submit" name="BTEnvia" value="Enviar" class="btn btn-primary"> -->
                        <button name="BTEnvia" id="name="BTEnvia" class="btn btn-primary">Enviar</button>
                      </div>
					  <div id="process" title="Mensagem"></div>	
                      <p id="test"></p>
		  </div>
		  <div class="col-xs-6" style="">
			   <div id='test'>
			  		<img src="banner.jpg" style="width: 100%" />
			   </div>
		  	<!-- <img src="banner.jpg" class="img-fluid" alt="Responsive image"> -->
		  </div>
		</div>
	</div>

	<div class="modal fade bs-example-modal-sm" id="modalTeste" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    	<div class="modal-dialog modal-sm" role="document">
	    	<div class="modal-content">
	            <div class="modal-header">
					
	            </div>
	            <div class="modal-body">
	            	<center><p>Email enviado com sucesso!</p></center>
	            </div>
	            <div class="modal-footer">
       				 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      			</div>
	        </div>
    	</div>
	</div>

	<script type="text/javascript">
		$("#txttelefone").mask("(00) 0000-00009");
	</script>

	<!-- <script type="text/javascript">
		$.ajax({
          type: 'POST',
          url: formEl.prop('action'),
          accept: {
            javascript: 'application/javascript'
          },
          data: formEl.serialize(),
          beforeSend: function() {
            submitButton.prop('disabled', 'disabled');
              }
        success: function(data) {
           $('#your-modal').modal('toggle');
          }
        }).done(function(data) {
          submitButton.prop('disabled', false);
        });
	</script> -->


</body>
</html>