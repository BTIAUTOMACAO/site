﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="boletos.aspx.cs" Inherits="GestaoEscolar.geProfessores.boletos" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Gestão Escolar - BTI</title>
    <link rel="stylesheet" type="text/css" href="../css/960.css" />
    <link rel="stylesheet" type="text/css" href="../css/reset.css" />
    <link rel="stylesheet" type="text/css" href="../css/text.css" />
    <link rel="stylesheet" type="text/css" href="../css/blue.css" />
    <!--<link type="text/css" href="../css/smoothness/ui.css" rel="stylesheet" />-->
    <script type="text/javascript" src="../../ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
    <script type="text/javascript" src="../js/ui.core.js"></script>
    <script type="text/javascript" src="../js/ui.sortable.js"></script>
    <script type="text/javascript" src="../js/ui.dialog.js"></script>
    <script type="text/javascript" src="../js/ui.datepicker.js"></script>
    <script type="text/javascript" src="../js/effects.js"></script>
    <script id="source" language="javascript" type="text/javascript" src="/js/graphs.js"></script>
</head>
<body>
    <form id="Form1" runat="server">
    <div class="container_16" id="wrapper">
        <!--LOGO-->
        <div class="grid_8" id="logo">
           Painel da Escola</div>
        <div class="grid_8">
            <!-- TOPO LADO DIREITO -->
            <div style="font-size: large; color: White; float: right; padding-top: 20px">
                Bem vindo 
                <asp:Label ID="lblUsuario" runat="server" Text=""></asp:Label>
                | 
                <asp:Button ID="btnSair" runat="server" Text="Sair" onclick="btnSair_Click" /></div>
            <div id="user_tools">
                <span><a href="#"></a></span>
            </div>
        </div>
        <div class="grid_16" id="header">
            <!-- MENU -->
            <div id="menu">
                <ul class="group" id="menu_group_main">
                    <li class="item first" id="one"><a href="#" class="main"><span class="outer"><img src="../images/icon_dashboard.png" onMouseOver="this.src='../images/icon_dashboard_mouse.png'" onmouseout="this.src='../images/icon_dashboard.png'"/><br />Frequência<br /> e Notas</span></span></a></li>
                    <li class="item middle" id="two"><a href="#" class="main"><span class="outer"><img src="../images/icon_media.png" onMouseOver="this.src='../images/icon_media_mouse.png'" onmouseout="this.src='../images/icon_media.png'"/><br />Extrato do<br />Cartão Cantina</span></a></li>
                    <li class="item middle" id="three"><a href="#" class="main"><span class="outer"><img src="../images/icon_reports.png" onMouseOver="this.src='../images/icon_reports_mouse.png'" onmouseout="this.src='../images/icon_reports.png'"/><br />Boletos</span></a></li>
                    <li class="item middle" id="four"><a href="biblioteca.aspx" class="main"><span class="outer"><img src="../images/icon_users.png" /><br />Biblioteca</span></a></li>
                    <li class="item middle" id="five"><a href="noticia.aspx" class="main"><span class="outer"><img src="../images/icon_news.png" /><br />Notícias</span></a></li>
                    <li class="item middle" id="six"><a href="psicoPedagogico.aspx" class="main"><span class="outer"><img src="../images/icon_clock.png" /><br />Apoio<br />PsicoPedagógico</span></a></li>
                    <li class="item middle" id="seven"><a href="formulario.aspx" class="main"><span class="outer"><img src="../images/icon_edit.png" /><br />Formulário de<br />Solicitações</span></a></li>
                    <li class="item last" id="eight"><a href="http://btiautomacao.com.br/moodle/" target="_blank" class="main"><span class="outer"><img src="../images/icon_settings.png" /><br />Ambiente<br />Virtual</span></a></li>
                </ul>
            </div>
            <!-- FIM MENU -->
        </div>
        <div class="grid_16">
            <!-- BOTÃO INICIO -->
            <div id="tabs">
                <div class="container">
                    <ul>
                        <li><a href="index.aspx" class="current"><span>Início</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
        
        <div class="grid_16" id="content">
            <!--  TITULO  -->
            <div class="grid_9">
                <h1 class="dashboard_bl">
                    Boletos</h1>
            </div>
            <div class="grid_6" id="eventbox">
                <a href="index.aspx" class="inline_calendar" style="padding-bottom: 5px;">
                    <asp:Label ID="lblData" runat="server" Text=""></asp:Label></a>
                <div class="hidden_calendar">
                </div>
            </div>
            <div class="clear">
            </div>

            <!-- COMEÇO DO CONTEÚDO -->
                <div class="portlet">
                    <div class="portlet-header fixed">
                        <img src="../images/icons/user.gif" width="16" height="16" alt="Latest Registered Users" />
                        Boletos pendentes</div>
                    <div class="portlet-content nopadding">
                        <form action="" method="post">
                        <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
                            <thead>
                                <tr>
                                    <th width="500" scope="col">
                                        Nome
                                    </th>
                                    
                                    <th width="400" scope="col">
                                        Data Vencimento
                                    </th>
                                    <th width="300" scope="col">
                                        Valor $
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                       Mensalidade
                                    </td>
                                    <td>
                                        07-05-2015
                                    </td>
                                    <td>
                                        150,00
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Cartão Cantina
                                    </td>
                                    <td>
                                        08-05-2015
                                    </td>
                                    <td>
                                        35,50
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        </form>
                    </div>
                </div>
            <!-- FIM DO CONTEÚDO -->

            </div>
            <div class="clear">
            </div>
        </div>
        <div class="clear">
        </div>
    <!-- FOOTER -->
    <div class="container_16" id="footer">
        Gestão Escolar <a href="index.aspx">BTI</a></div>
    <!-- FIM FOOTER -->
    </form>
</body>
</html>


