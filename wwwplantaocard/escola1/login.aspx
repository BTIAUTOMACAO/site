﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="GestaoEscolar.login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="css/login.css" rel="stylesheet" />
    <title>Login Gestão Escolar</title>
</head>
<body>
    <center>
        <img src="images/logo.png" />
    </center><br />
    <div class="container">
        <form id="signup">
            <div class="header">
                <center>
                    <h3>Acesse o Portal Acadêmico</h3>
                </center>
            </div>
            <div class="sep">
            </div>
            <div class="inputs">
            <form id="Form1" runat="server">
                Nome:
                <asp:TextBox ID="txtNome" runat="server" Height="30px" Width="350px"></asp:TextBox><br /><br />
                Senha:
                <asp:TextBox ID="txtSenha" runat="server" Height="30px" Width="350px"></asp:TextBox><br /><br />
                <center><asp:Label ID="lblAviso" runat="server" Text="Label" Visible="false" ForeColor="#FF3300"></asp:Label><br /><br /></center>        
                <asp:Button ID="btnAcessar" runat="server" Text="Acessar" 
                    onclick="btnAcessar_Click" Height="41px" Width="351px" />
            </form>
            </div>
        </form>
    </div>
</body>
</html>
