$(function(){
	$("#status").val("0");
})

function Buscar()
{
	var status;
	if($("#status option:selected").val() == "0"){
		status = "TODOS"
	}
 	else if($("#status option:selected").val() == "1"){
 		status ="S";
 	}
 	else
 	{
 		status ="N";
 	}

 	BuscarPromocoes(status);

 };

function BuscarPromocoes(liberado)
{
 	$.ajax({
 		type: "GET",
 		url: sessionStorage.getItem('endereco') + "Promocao/BuscaPromocoesPorCredId",
 		data:{	
 			credID:sessionStorage.getItem('credID'),
 			tipo: sessionStorage.getItem('tipo'),
 			liberado: liberado
 		},
 	    success: function (data) {
	   		if(data[0].codErro == "00"){
	   			$.each(data, function (i) {
                	var conteudo = ("<tr>" +
                		"<td>" + data[i].Titulo + "</td>" +
                		"<td>" + data[i].Categoria + "</td>" +
                		"<td>" + data[i].Liberado + "</td>" +
                		"<td>" + data[i].Fixo + "</td>" +
                		"<td>" + data[i].DataInicio + "</td>" +
                		"<td>" + data[i].DataFim + "</td>" +
                		"<td><img src=img/" + data[i].Imagem + " height=\"10%\" width=\"20%\" /></td>" +
                		"<td><a class='btn btn-warning btn-xs' href='excluirPromocao.html?id=" + data[i].ID + "'>Excluir</a></td>"+
                		"</tr>");

                	$("#listaPromocao").append(conteudo)
	   			})
	   		}else{
	   			alert(data[0].mensagem,"Atenção");
	   		}			
	   	},
 		error: function(){
 			alert("Erro ao tentar acessar o servidor","Atenção");
 		}

 	})
};
