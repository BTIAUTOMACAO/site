﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frequenciaNotas.aspx.cs" Inherits="GestaoEscolar.geAlunos.frequenciaNotas" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Gestão Escolar - BTI</title>
    <link rel="stylesheet" type="text/css" href="../css/960.css" />
    <link rel="stylesheet" type="text/css" href="../css/reset.css" />
    <link rel="stylesheet" type="text/css" href="../css/text.css" />
    <link rel="stylesheet" type="text/css" href="../css/blue.css" />
    <script type="text/javascript" src="../../ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
    <script type="text/javascript" src="../js/ui.core.js"></script>
    <script type="text/javascript" src="../js/ui.sortable.js"></script>
    <script type="text/javascript" src="../js/ui.dialog.js"></script>
    <script type="text/javascript" src="../js/ui.datepicker.js"></script>
    <script type="text/javascript" src="../js/effects.js"></script>
    <script id="source" language="javascript" type="text/javascript" src="/js/graphs.js"></script>
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
        th, td {
            padding: 5px;
        }
    </style>
</head>
<body>
    <form id="Form1" runat="server">
    <div class="container_16" id="wrapper">
        <!--LOGO-->
        <div class="grid_8" id="logo">
            Painel do Aluno</div>
        <div class="grid_8">
            <div style="font-size: large; color: White; float: right; padding-top: 20px">
                Bem vindo 
                <asp:Label ID="lblUsuario" runat="server" Text=""></asp:Label>
                | 
                <asp:Button ID="btnSair" runat="server" Text="Sair" onclick="btnSair_Click" /></div>
            <div id="user_tools">
                <span><a href="#"></a></span>
            </div>
        </div>
        <div class="grid_16" id="header">
            <!-- MENU -->
            <div id="menu">
                <ul class="group" id="menu_group_main">
                    <li class="item first" id="one"><a href="frequenciaNotas.aspx" class="main"><span class="outer"><img src="../images/icon_dashboard.png" onmouseup=""/><br />Frequência<br /> e Notas</span></span></a></li>
                    <li class="item middle" id="two"><a href="#" class="main"><span class="outer"><img src="../images/icon_media.png" onMouseOver="this.src='../images/icon_media_mouse.png'" onmouseout="this.src='../images/icon_media.png'"/><br />Extrato do<br />Cartão Cantina</span></a></li>
                    <li class="item middle" id="three"><a href="#" class="main"><span class="outer"><img src="../images/icon_reports.png" onMouseOver="this.src='../images/icon_reports_mouse.png'" onmouseout="this.src='../images/icon_reports.png'"/><br />Boletos</span></a></li>
                    <li class="item middle" id="four"><a href="biblioteca.aspx" class="main"><span class="outer"><img src="../images/icon_users.png" /><br />Biblioteca</span></a></li>
                    <li class="item middle" id="five"><a href="noticia.aspx" class="main"><span class="outer"><img src="../images/icon_news.png" /><br />Notícias</span></a></li>
                    <li class="item middle" id="six"><a href="psicoPedagogico.aspx" class="main"><span class="outer"><img src="../images/icon_clock.png" /><br />Apoio<br />PsicoPedagógico</span></a></li>
                    <li class="item middle" id="seven"><a href="formulario.aspx" class="main"><span class="outer"><img src="../images/icon_edit.png" /><br />Formulário de<br />Solicitações</span></a></li>
                    <li class="item last" id="eight"><a href="http://btiautomacao.com.br/moodle/" target="_blank" class="main"><span class="outer"><img src="../images/icon_settings.png" /><br />Ambiente<br />Virtual</span></a></li>
                </ul>
            </div>
            <!-- /MENU -->
        </div>
        <div class="grid_16">
            <div id="tabs">
                <div class="container">
                    <ul>
                        <li><a href="index.aspx" class="current"><span>Início</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="grid_16" id="content">
            <!--  TITULO  -->
            <div class="grid_9">
                <h1 class="dashboard_fn">
                    Controle de Frequência e Notas</h1>
            </div>
            <div class="grid_6" id="eventbox">
                <a href="index.aspx" class="inline_calendar" style="padding-bottom: 5px;">
                    <asp:Label ID="lblData" runat="server" Text=""></asp:Label></a>
                <div class="hidden_calendar">
                </div>
            </div>
            <div class="clear">
            </div>
                <div class="portlet">
                    <div class="portlet-header fixed">
                        <img src="../images/icons/user.gif" width="16" height="16" alt="Latest Registered Users" />
                        Controle de frequência e Notas escolares
                    </div><br />
                    <!--  DADOS DE FREQUENCIA -->
                    <center>
                        <asp:GridView ID="gvFrequencia" runat="server" AutoGenerateColumns="False" 
                            CellPadding="4" EnableModelValidation="True" GridLines="None" 
                            Width="534px" ForeColor="#333333">
                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                            <Columns>
                                <asp:TemplateField HeaderText="Data">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("datadia") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("datadia").ToString().Substring(0, 10) %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="entrada" HeaderText="Entrada" />
                                <asp:BoundField DataField="saida" HeaderText="Saida" />
                            </Columns>
                            <EditRowStyle BackColor="#999999" />
                            <FooterStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True" />
                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                        </asp:GridView>
                    </center>
                    <!--  /DADOS DE FREQUENCIA -->
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    <div class="container_16" id="footer">Gestão Escolar <a href="index.aspx">BTI</a></div>
    </form>
</body>
</html>
