﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="psicoPedagogico.aspx.cs" Inherits="GestaoEscolar.geProfessores.psicoPedagogico" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Gestão Escolar - BTI</title>
    <link rel="stylesheet" type="text/css" href="../css/960.css" />
    <link rel="stylesheet" type="text/css" href="../css/reset.css" />
    <link rel="stylesheet" type="text/css" href="../css/text.css" />
    <link rel="stylesheet" type="text/css" href="../css/blue.css" />
    <!--<link type="text/css" href="../css/smoothness/ui.css" rel="stylesheet" />-->
    <script type="text/javascript" src="../../ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
    <script type="text/javascript" src="../js/ui.core.js"></script>
    <script type="text/javascript" src="../js/ui.sortable.js"></script>
    <script type="text/javascript" src="../js/ui.dialog.js"></script>
    <script type="text/javascript" src="../js/ui.datepicker.js"></script>
    <script type="text/javascript" src="../js/effects.js"></script>
    <script id="source" language="javascript" type="text/javascript" src="/js/graphs.js"></script>
</head>
<body>
    <form id="Form1" runat="server">
    <!-- WRAPPER START -->
    <div class="container_16" id="wrapper">
        <!-- HIDDEN COLOR CHANGER -->
        <!--LOGO-->
        <div class="grid_8" id="logo">
            Painel do Professor</div>
        <div class="grid_8">
            <!-- USER TOOLS START -->
            <div style="font-size: large; color: White; float: right; padding-top: 20px">
                Bem vindo 
                <asp:Label ID="lblUsuario" runat="server" Text=""></asp:Label>
                | 
                <asp:Button ID="btnSair" runat="server" Text="Sair" onclick="btnSair_Click" /></div>
            <div id="user_tools">
                <span><a href="#"></a></span>
            </div>
        </div>
        <!-- USER TOOLS END -->
        <div class="grid_16" id="header">
            <!-- MENU START -->
            <div id="menu">
                <ul class="group" id="menu_group_main">
                    <li class="item first" id="one"><a href="#" class="main"><span class="outer"><img src="../images/icon_dashboard.png" onMouseOver="this.src='../images/icon_dashboard_mouse.png'" onmouseout="this.src='../images/icon_dashboard.png'"/><br />Frequência<br /> e Notas</span></span></a></li>
                    <li class="item middle" id="two"><a href="#" class="main"><span class="outer"><img src="../images/icon_media.png" onMouseOver="this.src='../images/icon_media_mouse.png'" onmouseout="this.src='../images/icon_media.png'"/><br />Extrato do<br />Cartão Cantina</span></a></li>
                    <li class="item middle" id="three"><a href="#" class="main"><span class="outer"><img src="../images/icon_reports.png" onMouseOver="this.src='../images/icon_reports_mouse.png'" onmouseout="this.src='../images/icon_reports.png'"/><br />Boletos</span></a></li>
                    <li class="item middle" id="four"><a href="biblioteca.aspx" class="main"><span class="outer"><img src="../images/icon_users.png" /><br />Biblioteca</span></a></li>
                    <li class="item middle" id="five"><a href="noticia.aspx" class="main"><span class="outer"><img src="../images/icon_news.png" /><br />Notícias</span></a></li>
                    <li class="item middle" id="six"><a href="psicoPedagogico.aspx" class="main"><span class="outer"><img src="../images/icon_clock.png" /><br />Apoio<br />PsicoPedagógico</span></a></li>
                    <li class="item middle" id="seven"><a href="formulario.aspx" class="main"><span class="outer"><img src="../images/icon_edit.png" /><br />(3) Novas <br /> Mensagens</span></a></li>
                    <li class="item last" id="eight"><a href="http://btiautomacao.com.br/moodle/" target="_blank" class="main"><span class="outer"><img src="../images/icon_settings.png" /><br />Ambiente<br />Virtual</span></a></li>
                </ul>
            </div>
            <!-- MENU END -->
        </div>
        <div class="grid_16">
            <!-- TABS START -->
            <div id="tabs">
                <div class="container">
                    <ul>
                        <li><a href="index.aspx" class="current"><span>Início</span></a></li>
                    </ul>
                </div>
            </div>
            <!-- TABS END -->
        </div>
        
        <!-- HIDDEN SUBMENU END -->
        <!-- CONTENT START -->
        <div class="grid_16" id="content">
            <!--  TITLE START  -->
            <div class="grid_9">
                <h1 class="dashboard_ap">
                    Apoio PsicoPedagógico</h1>
            </div>
            <!--RIGHT TEXT/CALENDAR-->
            <div class="grid_6" id="eventbox">
                <a href="index.aspx" class="inline_calendar" style="padding-bottom: 5px;">
                    <asp:Label ID="lblData" runat="server" Text=""></asp:Label></a>
                <div class="hidden_calendar">
                </div>
            </div>
            <!--RIGHT TEXT/CALENDAR END-->
            <div class="clear">
            </div>
            <!--THIS IS A WIDE PORTLET-->
                <div class="portlet">
                        <div class="portlet-header">
                            <img src="../images/icons/feed.gif" width="16" height="16" alt="Feeds" />
                            <strong>Psicopedagogia</strong>
                        </div>
                        <div class="portlet-content">
                            <ul class="news_items">
                                <li>Psicopedagogia é o campo do saber que se constrói a partir de dois saberes e práticas: a pedagogia e a psicologia. O campo dessa mediação recebe também influências da psicanálise, da lingüística, da semiótica, da neuropsicologia, da psicofisiologia, da filosofia humanista-existencial e da medicina.
                                    A psicopedagogia está intimamente ligada à psicologia educacional, da qual uma parte aplicada à prática1 . Ela diferencia-se da psicologia escolar, também esta uma subdisciplina da psicologia educacional, sob três aspectos2 :<br /><br />
                                    <strong>Quanto à origem</strong> - a psicologia escolar surgiu para compreender as causas do fracasso de certas crianças no sistema escolar enquanto a psicopedagogia surgiu para o tratamento de determinadas dificuldades de aprendizagem específicas;<br /><br />
                                    <strong>Quanto à formação</strong> - a psicologia escolar é uma especialização na área de psicologia, enquanto a psicopedagogia é aberta a profissionais de diferentes áreas;<br /><br />
                                    <strong>Quanto à atuação</strong> - a psicologia escolar é uma área propriamente psicológica enquanto a psicopedagogia é uma área plenamente interdisciplinar, tanto psicológica como pedagógica.
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="portlet">
                        <div class="portlet-header">
                            <img src="../images/icons/feed.gif" width="16" height="16" alt="Feeds" />
                            <strong>Atendimento psicopedagógico</strong>
                        </div>
                        <div class="portlet-content">
                            <ul class="news_items">
                                <li> O tratamento deve estar voltado para a construção do conhecimento, e não 
                                    para o produto final. São utilizados diversos recursos pelo psicopedagogo: 
                                    dramatizações, jogos, leituras, diálogos, desenhos, projetos e outras maneiras 
                                    que serão descobertas no decorrer dos atendimentos.  O psicopedagogo tem 
                                    informação para orientar pais e professores, ser o mediador de todo o processo e, 
                                    assim, ir além da junção do conhecimento do psicólogo e pedagogo.
                                </li><br />
                                <center>
                                    <img src="../images/psicopedagogico.jpg"
                                </center>
                            </ul>
                        </div>
                    </div>
            </div>
            <div class="clear">
            </div>
            <!-- END CONTENT-->
        </div>
            <div class="clear">
            </div>
            <!--  TITLE END  -->
            <!-- #PORTLETS START -->

    <!-- WRAPPER END -->
    <!-- FOOTER START -->
    <div class="container_16" id="footer">
        Gestão Escolar <a href="index.aspx">BTI</a></div>
    <!-- FOOTER END -->
    </form>
</body>
</html>
