<?php



?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="INDEX, FOLLOW" / >
    <meta name="language" content="portuguese" />
    <meta name="document-rights" content="Public" / >
    <meta name="document-rating" content="General" / >
    <meta name="document-state" content="Dynamic" / >
    <meta http-equiv="Content-Language" content="PT-BR" / >
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href='http://fonts.googleapis.com/css?family=Pontano+Sans' rel='stylesheet' type='text/css'>
    <meta name="description" content="<?=$var_description;?>">
    <meta name="keywords" content="<?=$palavra_chave;?>">
    <script type="text/javascript" src="assets/js/jquery-1.7.1.min.js"></script>
    <link rel="shortcut icon" type="image/ico" href="<?=$var_favicon;?>" />
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="assets/css/banner.css">
    <link rel="stylesheet" type="text/css" href="assets/css/topo_1.css">
    <link rel="stylesheet" type="text/css" href="assets/css/geral.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400" rel="stylesheet">
    <!-- <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet"> -->
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <script src="assets/js/jssor.slider-23.0.0.min.js" type="text/javascript"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDmsCjVC09sZS4qG87O1jE1rCCz42xgNsA" type="text/javascript"></script>
    <script src="assets/js/banner.js" type="text/javascript"></script>

    <!-- <link rel="stylesheet" type="text/css" href="assets/css/sreset.css"> -->
    <script src="assets/js/jquery-latest.js"></script>
    <script src="assets/js/js.js"></script>

        <title>Cartão Refeição - Plantão Card</title>


    <style type="text/css">

    .ttle_default{
      /*font-size: 38px;*/
      /*text-align: center;*/
      font-family: 'Raleway', sans-serif;
    }

    .ttle_default_sub{
      font-size: 20px;
      text-align: center;
      font-family: 'Raleway', sans-serif;
    }


    .int_map{
      position: absolute;
      z-index: 1000;
      background-color: #fff;
      padding:10px;
      margin-top: 50px;
      margin-left: 10px;
      border-radius:2px;
      border-color: #080808;
      border:none;
      -webkit-box-shadow: 0px 0px 20px rgba(0, 0, 0, 0.3);
      -moz-box-shadow:    0px 0px 20px rgba(0, 0, 0, 0.3);
      box-shadow:         0px 0px 20px rgba(0, 0, 0, 0.3);

    }

    .ttle_Card{
      font-family: 'Raleway', sans-serif;
      font-weight: bold;
      font-size: 30px;
      color: #999;
    }

    .p_padrao{
      padding-bottom: 40px;
      padding-top: 40px;
    }

    .p_0{
      padding: 0;
    }

    .home_res{
      max-width: 1364px;
    }

    .img_responsa{
      width: 100%;
    }

    .m_topo{
      color: #fff;
      font-size: 15px;
      float: left;
      right: 0%;
      padding-left: 15px;
      padding-right: 15px;
    }

    .mlat{
      border-right: #fff solid 1px;
    }

    .mmenu{
      position: absolute;
      right: 0%;
      max-width: 100%;
    }

    .bg_fff{
      background-color: #fff;
    }

    .font_25{
      font-size: 25px;
    }

    .font_15{
      color: #787878;
      font-size: 15px;
    }

    .span_1{
      color: #483c8c;
      font-weight: bold;
    }

    .span_2{
      color: #787878;
      font-weight: ;
    }

    .home_text{
      height: 500px;
    }

    .campo{
      padding-top: 15px;
      padding-bottom: 15px;
      text-indent: 0.8em;
      width: 90%;
      border-radius: 3px;
      background-color: #fff;
      border:#e5e5e5 solid 1px;
      font-size: 14px;
      margin-bottom: 10px;
    }.campo:focus{
      outline: none;
      border:#e0e0e0 solid 1px;
      background-color: #efefef;
    }
        .btn_contact{

        width: 90%;

        color: #707070;

        padding: 10px;
        border-radius: 2px;
        cursor: pointer;

    }.btn_contact:hover{
        opacity: 0.8;
    }

    .ttl_form{
      font-size: 18px;
      color: #666;
    }

    .backg{
      background-image: url(assets/img/bg_refeicao.png);
      background-repeat: no-repeat;
      background-position: right;
    }

    .font-t{
      font-size: 20px;
      color: #fff;
      font-family: 'Raleway', sans-serif;
    }

    .font-m{
      font-size: 17px;
      color: #fff;
      font-family: 'Raleway', sans-serif;
    }

    .ppd{
      padding-top: 40px;
      padding-bottom: 40px;
    }


    </style>



    <script>
    

    </script>


    </head>





<body>



    <?php include("_topo.php");?>




      <div class='container-fluid backg' style='background-color:#fec70b;'>
        <div class='container'>
            <div class='col-12 col-sm-2 col-md-2 col-lg-2 col-xl-2 text-center ppd'><img src='assets/img/cardRefeicao.png' class='' max-width='684' border='0'></div>
            <div class='col-12 col-sm-8 col-md-8 col-lg-8 col-xl-8 font-t ppd'>
            <b><span class='font-t'>CARTÃO REFEIÇÃO</span></b><br>

            <span class='font-m'>O Cartão Plantão Refeicão é um cartão que pode ser utilizado em restaurantes, lanchonetes e bares, para auxílio refeição do trabalhador. Conforme regulamentação do PAT.</span></div>
            
            <div class='col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4'></div>

        </div>
      </div>


      <div class='container-fluid p_padrao' style='background-color:#fff;'>
        <div class='container text-center'>
            <img src='assets/img/img_multiplo.png' class=''>
        </div>
      </div>


      <div class='container-fluid text-center bg_fff p_padrao'>
        <div class='container'>
          <div class='row ttle_Card'>CARTÕES PLANTÃO CARD</div>
          <div class='col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12'><br><br></div>
          <div class='row text-center'>
            <div class='col-12 col-sm-1 col-md-1 col-lg-1 col-xl-1'></div>
            <div class='col-12 col-sm-2 col-md-2 col-lg-2 col-xl-2'><a href='multiplo.php'><img src='assets/img/img_plantao.png' class='' max-width='684' border='0'></a></div>
            <div class='col-12 col-sm-2 col-md-2 col-lg-2 col-xl-2'><a href='combustivel.php'><img src='assets/img/img_combustivel.png' class='' max-width='684' border='0'></a></div>
            <div class='col-12 col-sm-2 col-md-2 col-lg-2 col-xl-2'><a href='refeicao.php'><img src='assets/img/img_refeicao.png' class='' max-width='684' border='0'></a></div>
            <div class='col-12 col-sm-2 col-md-2 col-lg-2 col-xl-2'><a href='alimentacao.php'><img src='assets/img/img_alimentacao.png' class='' max-width='684' border='0'></a></div>
            <div class='col-12 col-sm-2 col-md-2 col-lg-2 col-xl-2'><a href='natalino.php'><img src='assets/img/img_natalino.png' class='' max-width='684' border='0'></a></div>
            <div class='col-12 col-sm-1 col-md-1 col-lg-1 col-xl-1'></div>
            <div class='col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12'>
              <div style='max-width:625px; margin:0 auto;'><a href='contato.php'><img src='assets/img/solicitar.png' class='img_responsa' max-width='684' border='0'></a></div>
            </div>
          </div>
        </DIV>

      </div>




  <div class='container-fluid p_padrao' style='background-color:#e5e5e5;'>
      <div class='container'>
        <div class='col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 p_0 text-right font_25'><span class='span_1'>MILHARES DE<br>ESTABELECIMENTOS<br>ACEITAM PLANTÃO CARD.</span><bR><span class='span_2'>USE O SEU E APROVEITE<br>TODOS OS BENEFÍCIOS.</span></div>
        <div class='col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 font_15'>O Plantão Card funciona através de pagamentos<bR>
vinculados a desconto em folha de pagamentos.<bR>
Os cartões não tem custos para a empresa nem para os funcionários.<bR>
A Rede de Atendimento é composta por<bR>milhares de drogarias em todo o estado de São Paulo<bR>e milhares de outros estabelecimentos diversos, como supermercados,
drogarias, mercadinhos, hortifrutis, açougues, postos de combustíveis, lojas de calçados e roupas, óticas e muitos outros.</div>
      </div>
  </div>




    <?php include("footer.php");?>



</body>
</html>



<script src="assets/js/bootstrap_2.min.js"></script>
<script type="text/javascript">jssor_1_slider_init();</script>


