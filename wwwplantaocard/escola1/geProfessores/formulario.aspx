﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="formulario.aspx.cs" Inherits="GestaoEscolar.geProfessores.formulario" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Gestão Escolar - BTI</title>
    <link rel="stylesheet" type="text/css" href="../css/960.css" />
    <link rel="stylesheet" type="text/css" href="../css/reset.css" />
    <link rel="stylesheet" type="text/css" href="../css/text.css" />
    <link rel="stylesheet" type="text/css" href="../css/blue.css" />
    <!--<link type="text/css" href="../css/smoothness/ui.css" rel="stylesheet" />-->
    <script type="text/javascript" src="../../ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
    <script type="text/javascript" src="../js/ui.core.js"></script>
    <script type="text/javascript" src="../js/ui.sortable.js"></script>
    <script type="text/javascript" src="../js/ui.dialog.js"></script>
    <script type="text/javascript" src="../js/ui.datepicker.js"></script>
    <script type="text/javascript" src="../js/effects.js"></script>
    <script id="source" language="javascript" type="text/javascript" src="/js/graphs.js"></script>
</head>
<body>
    <form id="Form1" runat="server">
    <!-- WRAPPER START -->
    <div class="container_16" id="wrapper">
        <!-- HIDDEN COLOR CHANGER -->
        <!--LOGO-->
        <div class="grid_8" id="logo">
            Painel do Professor</div>
        <div class="grid_8">
            <!-- USER TOOLS START -->
            <div style="font-size: large; color: White; float: right; padding-top: 20px">
                Bem vindo 
                <asp:Label ID="lblUsuario" runat="server" Text=""></asp:Label>
                | 
                <asp:Button ID="btnSair" runat="server" Text="Sair" onclick="btnSair_Click" /></div>
            <div id="user_tools">
                <span><a href="#"></a></span>
            </div>
        </div>
        <!-- USER TOOLS END -->
        <div class="grid_16" id="header">
            <!-- MENU START -->
            <div id="menu">
                <ul class="group" id="menu_group_main">
                    <li class="item first" id="one"><a href="#" class="main"><span class="outer"><img src="../images/icon_dashboard.png" onMouseOver="this.src='../images/icon_dashboard_mouse.png'" onmouseout="this.src='../images/icon_dashboard.png'"/><br />Frequência<br /> e Notas</span></span></a></li>
                    <li class="item middle" id="two"><a href="#" class="main"><span class="outer"><img src="../images/icon_media.png" onMouseOver="this.src='../images/icon_media_mouse.png'" onmouseout="this.src='../images/icon_media.png'"/><br />Extrato do<br />Cartão Cantina</span></a></li>
                    <li class="item middle" id="three"><a href="#" class="main"><span class="outer"><img src="../images/icon_reports.png" onMouseOver="this.src='../images/icon_reports_mouse.png'" onmouseout="this.src='../images/icon_reports.png'"/><br />Boletos</span></a></li>
                    <li class="item middle" id="four"><a href="biblioteca.aspx" class="main"><span class="outer"><img src="../images/icon_users.png" /><br />Biblioteca</span></a></li>
                    <li class="item middle" id="five"><a href="noticia.aspx" class="main"><span class="outer"><img src="../images/icon_news.png" /><br />Notícias</span></a></li>
                    <li class="item middle" id="six"><a href="psicoPedagogico.aspx" class="main"><span class="outer"><img src="../images/icon_clock.png" /><br />Apoio<br />PsicoPedagógico</span></a></li>
                    <li class="item middle" id="seven"><a href="formulario.aspx" class="main"><span class="outer"><img src="../images/icon_edit.png" /><br />(3) Novas <br /> Mensagens</span></a></li>
                    <li class="item last" id="eight"><a href="http://btiautomacao.com.br/moodle/" target="_blank" class="main"><span class="outer"><img src="../images/icon_settings.png" /><br />Ambiente<br />Virtual</span></a></li>
                </ul>
            </div>
            <!-- MENU END -->
        </div>
        <div class="grid_16">
            <!-- TABS START -->
            <div id="tabs">
                <div class="container">
                    <ul>
                        <li><a href="index.aspx" class="current"><span>Início</span></a></li>
                    </ul>
                </div>
            </div>
            <!-- TABS END -->
        </div>
        
        <!-- HIDDEN SUBMENU END -->
        <!-- CONTENT START -->
        <div class="grid_16" id="content">
            <!--  TITLE START  -->
            <div class="grid_9">
                <h1 class="dashboard_frm">
                    Formulário de Solicitações</h1>
            </div>
            <!--RIGHT TEXT/CALENDAR-->
            <div class="grid_6" id="eventbox">
                <a href="index.aspx" class="inline_calendar" style="padding-bottom: 5px;">
                    <asp:Label ID="lblData" runat="server" Text=""></asp:Label></a>
                <div class="hidden_calendar">
                </div>
            </div>
            <!--RIGHT TEXT/CALENDAR END-->
            <div class="clear">
            </div>

            <!-- COMEÇO DO CONTEÚDO -->
                <div class="portlet">
                        <div class="portlet-header">
                            <img src="../images/icons/feed.gif" width="16" height="16" alt="Feeds" />
                            <strong>Nome: Marcos de Paula / Aluno: Fernando Luiz</strong>
                        </div>
                        <div class="portlet-content">
                            <ul class="news_items">
                                <strong>Assunto: Agradecimento</strong><br /><br />
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                Aenean adipiscing massa quis arcu interdum scelerisque. 
                                Duis vitae nunc nisi. Quisque eget leo a nibh gravida vulputate 
                                ut sed nulla. Donec quis lectus turpis, sed mollis nibh. Donec ut 
                                mi eu metus ultrices porttitor. Phasellus nec elit in nisi 
                                Nunc convallis, enim quis tincidunt dictum, ante ipsum interdum 
                                massa, consequat sodales arcu magna nec eros.
                        </div>
                    </div>

                    <div class="portlet">
                        <div class="portlet-header">
                            <img src="../images/icons/feed.gif" width="16" height="16" alt="Feeds" />
                            <strong>Nome: José Marques / Aluno: Camila de Paula</strong>
                        </div>
                        <div class="portlet-content">
                            <ul class="news_items">
                                <strong>Assunto: Trabalhos</strong><br /><br />
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                Aenean adipiscing massa quis arcu interdum scelerisque. 
                                Duis vitae nunc nisi. Quisque eget leo a nibh gravida vulputate 
                                ut sed nulla. Donec quis lectus turpis, sed mollis nibh. Donec ut 
                                mi eu metus ultrices porttitor. Phasellus nec elit in nisi 
                                Nunc convallis, enim quis tincidunt dictum, ante ipsum interdum 
                                massa, consequat sodales arcu magna nec eros.
                        </div>
                    </div>

                    <div class="portlet">
                        <div class="portlet-header">
                            <img src="../images/icons/feed.gif" width="16" height="16" alt="Feeds" />
                            <strong>Nome: Pedro Silva / Aluno: Anderson Oliveira</strong>
                        </div>
                        <div class="portlet-content">
                            <ul class="news_items">
                                <strong>Assunto: Faltas</strong><br /><br />
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                Aenean adipiscing massa quis arcu interdum scelerisque. 
                                Duis vitae nunc nisi. Quisque eget leo a nibh gravida vulputate 
                                ut sed nulla. Donec quis lectus turpis, sed mollis nibh. Donec ut 
                                mi eu metus ultrices porttitor. Phasellus nec elit in nisi 
                                Nunc convallis, enim quis tincidunt dictum, ante ipsum interdum 
                                massa, consequat sodales arcu magna nec eros.
                        </div>
                    </div>
            <!-- FIM DO CONTEÚDO -->

            </div>
            <div class="clear">
            </div>
            <!-- END CONTENT-->
        </div>
            <div class="clear">
            </div>
    <div class="container_16" id="footer">
        Gestão Escolar <a href="index.aspx">BTI</a></div>
    <!-- FOOTER END -->
    </form>
</body>
</html>

