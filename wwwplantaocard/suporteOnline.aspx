﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="pt-br"> <!--<![endif]-->
<head>

    <!-- Basic Page Needs
  ================================================== -->
	<meta charset="utf-8">
	<title>Suporte Online</title>
	<meta name="description" content="Suporte Online | Drogabella">
	<meta name="author" content="www.zerotheme.com">
	
    <!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    
    <!-- CSS
  ================================================== -->
	<link rel="stylesheet" href="css/suporte/zerogrid.css">
	<link rel="stylesheet" href="css/suporte/style.css">
    <link rel="stylesheet" href="css/suporte/responsive.css">
	<link rel="stylesheet" href="css/suporte/responsiveslides.css" />
	
	<!--[if lt IE 8]>
       <div style=' clear: both; text-align:center; position: relative;'>
         <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
           <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
      </div>
    <![endif]-->
    <!--[if lt IE 9]>
		<script src="js/html5.js"></script>
		<script src="js/css3-mediaqueries.js"></script>
	<![endif]-->
	
	<link href='css/images/favicon.png' rel='icon' type='image/x-icon'/>
    <script src="js/jquery.min.js"></script>
	<script src="js/responsiveslides.js"></script>
	<script>
	    $(function () {
	        $("#slider").responsiveSlides({
	            auto: true,
	            pager: false,
	            nav: true,
	            speed: 500,
	            maxwidth: 962,
	            namespace: "centered-btns"
	        });
	    });
	</script>
    <style type="text/css">
        .divider{background: url(css/images/suporte/divider.png) repeat-x; height: 7px; width: 90%; margin: auto;}
    </style>
</head>
<body>
<!--------------Header--------------->
<header>
	<div class="wrap-header zerogrid" style="height: 110px;">
		<div id="logo"><img src="css/images/suporte/logo.png"/></div>
			<br/>
			<div style="float:right; font-size:x-large">
				<strong>Telefone:</strong> (12) 3938-3633
			</div>
			<br />
			<div style="float:right">
				<a href="mailto:recepcao@drogabella.com.br">
					<strong>Email:</strong> recepcao@drogabella.com.br
				</a>
			</div>
		<nav>
			<div class="wrap-nav">
				<div class="menu">
					<ul>
						<li class="current"><a href="index.html">Voltar</a></li>
					</ul>
				</div>
				<div class="minimenu"><div>MENU</div>
					<select onchange="location=this.value">
						<option></option>
						<option value="contato.aspx">Voltar</option>
					</select>
				</div>
			</div>
		</nav>
	</div>
</header>

<div class="featured">
	<div class="wrap-featured zerogrid">
		<div class="slider">
			<div class="rslides_container">
				<ul class="rslides" id="slider">
					<li><img src="css/images/suporte/slide1.png"/></li>
				</ul>
			</div>
		</div>
	</div>
</div>

<!--------------Content--------------->
<section id="content">
	<div class="wrap-content zerogrid">
		
		<div class="row block01"><h2>Tire todas as suas dúvidas no atendimento <a>online</a></h2>
		<p>De segunda a sexta das 8:00h as 18:00h. O atendimento on-line é fácil e prático e pode ser acessado clicando na imagem logo abaixo:</p>
         <center>
         <iframe style="WIDTH: 108px; HEIGHT: 71px" 
            src="http://drogabella.mysuite.com.br/empresas/dro/verifica.php"
            frameborder=0 scrolling=no>            
        </iframe>
        </center>
		</div>
		<div class="divider"></div>
		<div class="row block01"><h2>Help-Desk</h2>
		    <p>Colocamos também a sua disposição um sistema de help-desk, que pode ser acessado logo abaixo, onde você cliente poderá realizar solicitações referente ao seu sistema e terá um acompanhamento de cada fase do processo.</p>
            <p><a href="http://drogabella.mysuite.com.br/central.php">Clique aqui para acessar o Help-Desk</a> (necessário ser um usuário cadastrado).</p>
        </div>
	</div>
</section>
<!--------------Footer--------------->
<footer>
	<div class="copyright" style="height: 10px;">
		<p>Copyright © 2014 BTI - Todos os direitos reservados.</p>
	</div>
</footer>
</body></html>