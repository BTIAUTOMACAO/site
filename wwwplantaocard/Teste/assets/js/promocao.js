$(function(){
	sessionStorage.setItem('endereco', "http://ts1.drogabella.com/apiPromocoes/api/");
	BuscaSegmentos();
 	BuscaCategorias();
	BuscaCidades();
})


function  BuscaSegmentos(){
    $.ajax({
        type: "GET",
        url: sessionStorage.getItem('endereco') + "Promocao/BuscaSegmentos",
        success: function (data) {
            if(data[0].codErro == "00"){
                $.each(data, function(i){
                    var segmento = ("<option value=" + data[i].ID + ">" + data[i].Descricao +  "</option>");
                    $('#segmento').append(segmento);
                })
            }else{
                alert(data[0].mensagem,"Atenção");
            }           
        },
        error: function(){
            alert("Erro ao tentar acessar o servidor","Atenção");
        }

       })
}

function  BuscaCategorias(){
    $.ajax({
        type: "GET",
        url: sessionStorage.getItem('endereco') + "Promocao/BuscaCategorias",
        success: function (data) {
            if(data[0].codErro == "00"){
                $.each(data, function(i){
                    var categoria = ("<option value=" + data[i].Descricao + ">" + data[i].Descricao +  "</option>");
                    $('#categoria').append(categoria);
                })
            }else{
                alert(data[0].mensagem,"Atenção");
            }           
        },
        error: function(){
            alert("Erro ao tentar acessar o servidor","Atenção");
        }

       })
}

function  BuscaCidades(){
    $.ajax({
        type: "GET",
        url: sessionStorage.getItem('endereco') + "Promocao/BuscaCidades",
        success: function (data) {
            if(data[0].codErro == "00"){
                $.each(data, function(i){
                    var cidade = ("<option value=" + data[i].ID + ">" + data[i].Nome +  "</option>");
                    $('#cidade').append(cidade);
                })
            }else{
                alert(data[0].mensagem,"Atenção");
            }           
        },
        error: function(){
            alert("Erro ao tentar acessar o servidor","Atenção");
        }

       })
}


function Buscar()
{
	document.getElementById('listaPromocoes').innerHTML = ""; 

	var status;
	if($("#status option:selected").val() == "0"){
		status = "TODOS"
	}
 	else if($("#status option:selected").val() == "1"){
 		status ="S";
 	}
 	else
 	{
 		status ="N";
 	}

 	BuscaPromocoes($("#segmento option:selected").val(), $("#estabelecimento").val(), $("#categoria option:selected").val(), $("#cidade option:selected").val());

 };

function  BuscaPromocoes(segmento, loja, catetoria, cidade){
    $.ajax({
        type: "GET",
        url: sessionStorage.getItem('endereco') + "Promocao/BuscaPromocoes",
        data:{	
 			Segmento: segmento,
 			Loja: loja,
 			Categoria: catetoria,
 			Cidade: cidade
 		},
        success: function (data) {
            if(data[0].codErro == "00"){
            	var contador = 1;
            	var lista = 1;
            	var listaPromocao = 1
                $.each(data, function(i){
                	if(contador == 1)
                	{
                		var cabecalho = ("<div class=\"row\">" + 
      									 "<ul class=\"thumbnails list-unstyled\" id=\"lista" + lista + "\">");

                			$("#listaPromocoes").append(cabecalho);
                	}

  					if(listaPromocao <= 4)
  					{
  							var conteudo = ("<li class=\"col-md-3\">" + 
							        "<div class=\"thumbnail\" style=\"padding: 0\">" + 
								        "<div style=\"padding:4px\">" + 
								        "<img style=\"width: 100%\" height=\"30%\" <img src=SitePromocoes/img/" + data[i].Imagem + ">" + 
								        "</div>" + 
								        "<div class=\"caption\">" + 
								        "<h3>" + data[i].Titulo + "</h3>" + 
								        "<h4>" + data[i].Descricao + "</h4>" + 
								        "<p>Loja: " + data[i].Fantasia + "</p>" + 
								        "<p>Categoria: " + data[i].Categoria + "</p>" +  
								        "<p>Telefone: " + data[i].Telefone + "</p>" + 
								        "<p><i class=\"icon icon-map-marker\"></i>" + data[i].Endereco + "," +  data[i].Numero + " - " + data[i].Bairro + "</p>" + 
								        "<p><i class=\"icon icon-map-marker\"></i>" + data[i].Cep + " - " +  data[i].Nome + "/" + data[i].Uf + "</p>" + 
								        "<h4><center>UTILIZE O VOUCHER</center></h4>" + 
								        "<h4><center>" + data[i].Voucher + "</center></h4>" + 
								        "</div>" + 
							        "</div>" + 
							        "</li>");

			                	$("#lista" + lista).append(conteudo);

			                	
  					}

  					if(listaPromocao == 4)
  					{
  						var rodape = ("</ul>" + 
			    					"</div>");

                			$("#listaPromocoes").append(rodape);
                			contador = 1;
                			lista = lista + 1;
                			listaPromocao = 1;
  					}
  					else 
  					{
                   		listaPromocao = listaPromocao + 1;
                   	}
                })
            }else{
                alert(data[0].mensagem,"Atenção");
            }           
        },
        error: function(){
            alert("Erro ao tentar acessar o servidor","Atenção");
        }

       })
}
