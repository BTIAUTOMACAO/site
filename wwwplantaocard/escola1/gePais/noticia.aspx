﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="noticia.aspx.cs" Inherits="GestaoEscolar.gePais.noticia" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Gestão Escolar - BTI</title>
    <link rel="stylesheet" type="text/css" href="../css/960.css" />
    <link rel="stylesheet" type="text/css" href="../css/reset.css" />
    <link rel="stylesheet" type="text/css" href="../css/text.css" />
    <link rel="stylesheet" type="text/css" href="../css/blue.css" />
    <script type="text/javascript" src="../../ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
    <script type="text/javascript" src="../js/ui.core.js"></script>
    <script type="text/javascript" src="../js/ui.sortable.js"></script>
    <script type="text/javascript" src="../js/ui.dialog.js"></script>
    <script type="text/javascript" src="../js/ui.datepicker.js"></script>
    <script type="text/javascript" src="../js/effects.js"></script>
    <script id="source" language="javascript" type="text/javascript" src="/js/graphs.js"></script>
</head>
<body>
    <form id="Form1" runat="server">
    <div class="container_16" id="wrapper">
        <!--LOGO-->
        <div class="grid_8" id="logo">Painel dos Pais</div>
        <div class="grid_8">
            <div style="font-size: large; color: White; float: right; padding-top: 20px">
                Bem vindo 
                <asp:Label ID="lblUsuario" runat="server" Text=""></asp:Label>
                | 
                <asp:Button ID="btnSair" runat="server" Text="Sair" onclick="btnSair_Click" /></div>
        </div>
        <div class="grid_16" id="header">
            <!-- MENU -->
            <div id="menu">
                <ul class="group" id="menu_group_main">
                    <li class="item first" id="one"><a href="frequenciaNotas.aspx" class="main"><span class="outer"><img src="../images/icon_dashboard.png" /><br />Frequência<br /> e Notas</span></span></a></li>
                    <li class="item middle" id="two"><a href="cartaoCantina.aspx" class="main"><span class="outer"><img src="../images/icon_media.png" /><br />Extrato do<br />Cartão Cantina</span></a></li>
                    <li class="item middle" id="three"><a href="boletos.aspx" class="main"><span class="outer"><img src="../images/icon_reports.png" /><br />Boletos</span></a></li>
                    <li class="item middle" id="four"><a href="biblioteca.aspx" class="main"><span class="outer"><img src="../images/icon_users.png" /><br />Biblioteca</span></a></li>
                    <li class="item middle" id="five"><a href="noticia.aspx" class="main"><span class="outer"><img src="../images/icon_news.png" /><br />Notícias</span></a></li>
                    <li class="item middle" id="six"><a href="psicoPedagogico.aspx" class="main"><span class="outer"><img src="../images/icon_clock.png" /><br />Apoio<br />PsicoPedagógico</span></a></li>
                    <li class="item middle" id="seven"><a href="formulario.aspx" class="main"><span class="outer"><img src="../images/icon_edit.png" /><br />Formulário de<br />Solicitações</span></a></li>
                    <li class="item last" id="eight"><a href="http://btiautomacao.com.br/moodle/" target="_blank" class="main"><span class="outer"><img src="../images/icon_settings.png" /><br />Ambiente<br />Virtual</span></a></li>
                </ul>
            </div>
            <!-- MENU END -->
        </div>
        <div class="grid_16">
            <!-- TABS START -->
            <div id="tabs">
                <div class="container">
                    <ul>
                        <li><a href="index.aspx" class="current"><span>Início</span></a></li>
                    </ul>
                </div>
            </div>
            <!-- TABS END -->
        </div>
        
        <!-- HIDDEN SUBMENU END -->
        <!-- CONTENT START -->
        <div class="grid_16" id="content">
            <!--  TITLE START  -->
            <div class="grid_9">
                <h1 class="dashboard_n">
                    Notícias</h1>
            </div>
            <!--RIGHT TEXT/CALENDAR-->
            <div class="grid_6" id="eventbox">
                <a href="index.aspx" class="inline_calendar" style="padding-bottom: 5px;">
                    <asp:Label ID="lblData" runat="server" Text=""></asp:Label></a>
                <div class="hidden_calendar">
                </div>
            </div>
            <!--RIGHT TEXT/CALENDAR END-->
            <div class="clear">
            </div>
            <!--THIS IS A WIDE PORTLET-->
                <div class="portlet">
                        <div class="portlet-header">
                            <img src="../images/icons/feed.gif" width="16" height="16" alt="Feeds" />
                            <strong>Palestras na Escola</strong>
                        </div>
                        <div class="portlet-content">
                            <ul class="news_items">
                                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean adipiscing massa
                                    quis arcu interdum scelerisque. Duis vitae nunc nisi. Quisque eget leo a nibh gravida
                                    vulputate ut sed nulla. <a href="#">Donec quis lectus turpis, sed mollis nibh</a>.
                                    Donec ut mi eu metus ultrices porttitor. Phasellus nec elit in nisi</li>
                                <li>Nunc convallis, enim quis tincidunt dictum, ante ipsum interdum massa, consequat
                                    sodales arcu magna nec eros.<a href="#"> Vivamus nec placerat odio.</a> Sed nec
                                    mi sed orci mattis feugiat. Etiam est dui, rutrum nec dictum vel, accumsan id sem.
                                </li>
                                <li>Nunc convallis, enim quis tincidunt dictum, ante ipsum interdum massa, consequat
                                    sodales arcu magna nec eros.<a href="#"> Vivamus nec placerat odio.</a> Sed nec
                                    mi sed orci mattis feugiat. Etiam est dui, rutrum nec dictum vel, accumsan id sem.
                                </li>
                                <li>Nunc convallis, enim quis tincidunt dictum, ante ipsum interdum massa, consequat
                                    sodales arcu magna nec eros.<a href="#"> Vivamus nec placerat odio.</a> Sed nec
                                    mi sed orci mattis feugiat. Etiam est dui, rutrum nec dictum vel, accumsan id sem.
                                </li>
                                <li>Nunc convallis, enim quis tincidunt dictum, ante ipsum interdum massa, consequat
                                    sodales arcu magna nec eros.<a href="#"> Vivamus nec placerat odio.</a> Sed nec
                                    mi sed orci mattis feugiat. </li>
                            </ul>
                            <a href="#">&raquo; View all news items</a>
                        </div>
                    </div>
                    <div class="portlet">
                        <div class="portlet-header">
                            <img src="../images/icons/feed.gif" width="16" height="16" alt="Feeds" />
                            <strong>Novidades</strong>
                        </div>
                        <div class="portlet-content">
                            <ul class="news_items">
                                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean adipiscing massa
                                    quis arcu interdum scelerisque. Duis vitae nunc nisi. Quisque eget leo a nibh gravida
                                    vulputate ut sed nulla. <a href="#">Donec quis lectus turpis, sed mollis nibh</a>.
                                    Donec ut mi eu metus ultrices porttitor. Phasellus nec elit in nisi</li>
                                <li>Nunc convallis, enim quis tincidunt dictum, ante ipsum interdum massa, consequat
                                    sodales arcu magna nec eros.<a href="#"> Vivamus nec placerat odio.</a> Sed nec
                                    mi sed orci mattis feugiat. Etiam est dui, rutrum nec dictum vel, accumsan id sem.
                                </li>
                                <li>Nunc convallis, enim quis tincidunt dictum, ante ipsum interdum massa, consequat
                                    sodales arcu magna nec eros.<a href="#"> Vivamus nec placerat odio.</a> Sed nec
                                    mi sed orci mattis feugiat. Etiam est dui, rutrum nec dictum vel, accumsan id sem.
                                </li>
                                <li>Nunc convallis, enim quis tincidunt dictum, ante ipsum interdum massa, consequat
                                    sodales arcu magna nec eros.<a href="#"> Vivamus nec placerat odio.</a> Sed nec
                                    mi sed orci mattis feugiat. Etiam est dui, rutrum nec dictum vel, accumsan id sem.
                                </li>
                                <li>Nunc convallis, enim quis tincidunt dictum, ante ipsum interdum massa, consequat
                                    sodales arcu magna nec eros.<a href="#"> Vivamus nec placerat odio.</a> Sed nec
                                    mi sed orci mattis feugiat. </li>
                            </ul>
                            <a href="#">&raquo; View all news items</a>
                        </div>
                    </div>
            </div>
            <div class="clear">
            </div>
            <!-- END CONTENT-->
        </div>
            <div class="clear">
            </div>
            <!--  TITLE END  -->
            <!-- #PORTLETS START -->

    <!-- WRAPPER END -->
    <!-- FOOTER START -->
    <div class="container_16" id="footer">
        Gestão Escolar <a href="index.aspx">BTI</a></div>
    <!-- FOOTER END -->
    </form>
</body>
</html>

